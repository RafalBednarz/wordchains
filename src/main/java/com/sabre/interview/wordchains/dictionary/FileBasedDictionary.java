package com.sabre.interview.wordchains.dictionary;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileBasedDictionary implements Dictionary {

	public static final String FILENAME = "wordlist.txt";
	
    public List<String> getAllWordsOfGivenLengthFromDictionary(int lenghtOfWord) {
    	List<String> lines;
		try {
			FileReader fileReader = new FileReader(FILENAME);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			lines = new ArrayList<String>();
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				if(line.length()==lenghtOfWord) {
					lines.add(line);
				}
			}
			bufferedReader.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} 
        return lines;
    }
}