package com.sabre.interview.wordchains.dictionary;

import java.util.List;

public interface Dictionary {
	public List<String> getAllWordsOfGivenLengthFromDictionary(int lenghtOfWord);
}
