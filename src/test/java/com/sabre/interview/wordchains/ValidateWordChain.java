package com.sabre.interview.wordchains;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.sabre.interview.wordchains.dictionary.Dictionary;
import com.sabre.interview.wordchains.dictionary.FileBasedDictionary;
import com.sabre.interview.wordchains.exception.WordChainNotFoundException;
import com.sabre.interview.wordchains.exception.WordDoesNotExistInDictionaryException;
import com.sabre.interview.wordchains.exception.WordsAreNotOfTheSameLengthException;


public class ValidateWordChain {

	@Test
	public void testIfReturnsList() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		assertTrue(wordChains.getWordChain("cat", "dog") instanceof List);
	}

	@Test(expected = WordsAreNotOfTheSameLengthException.class) 
	public void testForWordsWithDifferentLength() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		wordChains.getWordChain("cat", "servings");
	}
	
	@Test
	public void testForTheSameWord() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		assertEquals(Arrays.asList("cat"), wordChains.getWordChain("cat", "cat"));
	}
	
	@Test(expected = WordDoesNotExistInDictionaryException.class) 
	public void testForStartWordWhichDoesNotExistInDictionary() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		wordChains.getWordChain("blablabla", "servilely");
	}
	
	@Test(expected = WordDoesNotExistInDictionaryException.class) 
	public void testForEndWordWhichDoesNotExistInDictionary() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		wordChains.getWordChain("servilely", "blablabla");
	}
	
	@Test
	public void testForWordsWhichDiffersWithOneLetterOnly() {
		Dictionary dictionary=new FileBasedDictionary();
		WordChains wordChains = new WordChains(dictionary);
		assertEquals(Arrays.asList("AB", "AC"), wordChains.getWordChain("AB", "AC"));
	}
	
	@Test
	public void testForChainOfWordsInMockDictionary() {
		
		Dictionary mockDictionary = new Dictionary() {
			@Override
			public List<String> getAllWordsOfGivenLengthFromDictionary(int lenghtOfWord) {
				return Arrays.asList("cat", "cot", "dot", "dog");
			}
		};
		
		WordChains wordChains = new WordChains(mockDictionary);
		List<String> chainOfWords = wordChains.getWordChain("cat", "dog");
		
		assertEquals(Arrays.asList("cat", "cot", "dot","dog"), chainOfWords);
	}
	
	@Test(expected = WordChainNotFoundException.class)
	public void testForWordChainThatDoesNotExist() {
		
		Dictionary mockDictionary = new Dictionary() {
			@Override
			public List<String> getAllWordsOfGivenLengthFromDictionary(int lenghtOfWord) {
				return Arrays.asList("cat", "cot", "dog");
			}
		};
		
		WordChains wordChains = new WordChains(mockDictionary);
		List<String> chainOfWords = wordChains.getWordChain("cat", "dog");
		
		assertEquals(Arrays.asList("cat", "cot", "dot","dog"), chainOfWords);
	}
	
	@Test
	public void testIfShortestPathIsChosen() {
		
		Dictionary mockDictionary = new Dictionary() {
			@Override
			public List<String> getAllWordsOfGivenLengthFromDictionary(int lenghtOfWord) {
				return Arrays.asList("dog", "dag", "gag", "rag", "dag");
			}
		};
		WordChains wordChains = new WordChains(mockDictionary);
		List<String> chainOfWords = wordChains.getWordChain("dog", "rag");
		assertEquals(Arrays.asList("dog", "dag", "rag"), chainOfWords);
	}

}
